# Service A 的 Dockerfile
# 准备及编译阶段使用golang:1.16作为基础镜像
FROM golang:1.16 as builder

# 使用go mod及代理
ENV GO111MODULE=on \
    GOPROXY=https://goproxy.cn,direct

# 明确工作目录
WORKDIR /tmp

# 拉取项目、获取依赖、编译代码并统一输出
RUN git clone https://gitlab.cn/jihulab/jh-infra/hands-on-tests/service-a.git && cd service-a && go mod tidy && CGO_ENABLED=0 go build && mkdir output && cp service-a output && cp config.yaml output

# 运行阶段指定alpine:3.15作为基础镜像
FROM alpine:3.15

# 明确工作目录
WORKDIR /run

# 拷贝程序及配置文件
COPY --from=builder /tmp/service-a/output .

# 声明监听端口
EXPOSE 8888

# 启动服务
ENTRYPOINT ["./service-a", "--file", "config.yaml"]