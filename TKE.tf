# 使用Terraform 代码实现创建腾讯云TKE
# 手册地址:https://registry.terraform.io/providers/tencentcloudstack/tencentcloud/latest/docs

# 调用腾讯云SDK
terraform {
  required_providers {
    tencentcloud = {
      source = "tencentcloudstack/tencentcloud"
      version = "1.60.20"
    }
  }
}

# 配置腾讯云API密钥,真实环境应使用TENCENTCLOUD_SECRET_ID,TENCENTCLOUD_SECRET_KEY等环境变量
provider "tencentcloud" {
  secret_id  = "my-secret-id"
  secret_key = "my-secret-key"
}

# 定义默认所在区域
variable "region" {
  default = "ap-guangzhou"
}

# 定义默认可用区
variable "availability_zone" {
  default = "ap-guangzhou-3"
}

# 定义VPC网络
resource "tencentcloud_vpc" "vpc01" {
  name         = "hands-on-test-vpc"
  cidr_block   = "192.168.0.0/16"
  dns_servers  = ["114.114.114.114", "8.8.8.8"]
  is_multicast = false
}

# 定义VPC子网
resource "tencentcloud_subnet" "subnet01" {
  availability_zone = var.availability_zone
  name              = "serviceA-subnet"
  vpc_id            = tencentcloud_vpc.vpc01.id
  cidr_block        = "192.168.1.0/24"
  is_multicast      = false
}

# 定义默认集群网络
variable "cluster_cidr" {
  default = "192.168.1.0/24"
}

# 定义默认实例类型
variable "default_instance_type" {
  default = "S2.MEDIUM4"
}

# 配置k8s集群
resource "tencentcloud_kubernetes_cluster" "cluster01" {
  vpc_id = tencentcloud_vpc.vpc01.id
  cluster_cidr = var.cluster_cidr
  cluster_name = "service A"
  cluster_desc = "a hands-on test"
  cluster_max_pod_num = 32
  cluster_max_service_num = 32
  cluster_internet = true
  managed_cluster_internet_security_policies = ["0.0.0.0"]
  cluster_deploy_type = "MANAGED_CLUSTER"

  worker_config {
    count = 1
    availability_zone  = var.availability_zone
    instance_type = var.default_instance_type
    system_disk_type = "CLOUD_SSD"
    system_disk_size = 50
    internet_charge_type = "TRAFFIC_POSTPAID_BY_HOUR"
    internet_max_bandwidth_out = 100
    public_ip_assigned = true
    subnet_id = tencentcloud_subnet.subnet01.subnet_id
    key_ids = "ssh-key-id"
  }
}